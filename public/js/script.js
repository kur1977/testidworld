$(function () {
    const STATUS_CREATED = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_FAILED = 3;
    const STATUS_SUCCESS = 4;

    let files_count = 0;
    let work = new Interval(doWork, 5000);


    //функция проверки  - выполняется ли в данный мемент ajax запрос
    //нужна для того, чтобы дождаться завершения и только потом отправить на сервер запрос на восстановление бекапа
    function checkPendingRequest() {
        if ($.active > 0) {
            window.setTimeout(checkPendingRequest(), 1000);
        } else {
            doBackup();
        }
    };

    //класс для удобного управления JS Interval
    //используется для последовательного запуска импорта загруженных файлов
    function Interval(fn, time) {
        let timer = false;
        this.start = function () {
            if (!this.isRunning())
                timer = setInterval(fn, time);
        };
        this.stop = function () {
            clearInterval(timer);
            timer = false;
        };
        this.isRunning = function () {
            return timer !== false;
        };
    }


    //запускается пока не получит в ответ, что все задачи на импорт завершены
    function doWork() {
        let jobs = [];

        $('#fileList div[data-job]').each(function () {
            jobs.push($(this).attr('data-job'));
        });

        $.ajax({
            url: '/index.php?mode=import',
            type: 'POST',
            data: {jobs},
            dataType: 'json',
            success: function (json) {
                let need_more = false;
                for (let key in json.info) {
                    let id = json.info[key].id;
                    let status = json.info[key].status;
                    if (status == STATUS_PROCESSING) {
                        need_more = true;
                    } else if (status == STATUS_PROCESSING) {
                        need_more = true;
                        $(`#fileList div[data-job="${id}"] span`)
                            .replaceWith('<span class="blink-text text-warning">файл импортируется</span>');
                    } else if (status == STATUS_FAILED) {
                        $(`#fileList div[data-job="${id}"]`).removeAttr('data-job').find('span')
                            .replaceWith('<span class="text-danger">Ошибка импорта</span>');
                    } else if (status == STATUS_SUCCESS) {
                        $(`#fileList div[data-job="${id}"]`).removeAttr('data-job').find('span')
                            .replaceWith(`<span class="text-success">Файл импортирован</span>
                                <span>Добавлено: ${json.info[key].inserted}</span>
                                <span>Обновлено: ${json.info[key].updated}</span>
                                <span>Ошибок валидации: ${json.info[key].errors}</span>
                                <a href="/index.php?mode=download&id=${id}">Скачать файл импорта</a>`);
                    }
                }

                if (!need_more) work.stop();
            }
        });

        if (!work.isRunning()) {
            work.start();
        }
    }

    $('#file').change(function () {

       // $('#sendFile').attr('disabled', true);
       // $('#restore').attr('disabled', true);

        if ($(this).val() !== '') {
            $('#sendFile').removeAttr('disabled');
        }
    });


    //восстановим бекап таблицы, который создан перед началом всех импортов
    function doBackup(){
        $.ajax({
            url: '/index.php?mode=restore',
            type: 'POST',
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                $('#sendFile').attr('disabled', true);
                $('#restore').attr('disabled', true);
                $('#fileList').html('<span>Изменения отменены</span>');
            }
        });
    }

    $('#restore').click(function () {
        if (work.isRunning()) {
            work.stop();
            checkPendingRequest();
        }else{
            doBackup();
        }
    });


    //загрузка файла на сервер
    $('#sendFile').click(function () {
        $('#restore').removeAttr('disabled');

        let formData = new FormData();
        let filedata = $('#file')[0].files[0];

        $('#fileList').append(`<div class="row" data-count="${files_count}">
            <div class="col">${filedata.name} <span class="blink-text text-info">файл загружается</span></div>
            </div>`);

        formData.append('file', filedata);

        $('#file').val('').trigger('change');

        let counter = files_count;

        $.ajax({
            url: '/index.php?mode=upload',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                let json = JSON.parse(data);
                if (json.error) {
                    $(`#fileList div[data-count="${counter}"] span`)
                        .replaceWith(`<span class="text-danger">${json.error}</span>`);
                }
                if (json.job_id) {
                    $(`#fileList div[data-count="${counter}"]`).attr('data-job', json.job_id);

                    doWork();
                }
            }
        });
        files_count++;
    });
});