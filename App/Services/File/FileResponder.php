<?php

namespace Services\File;

use Services\Job\Worker;

class FileResponder
{
    private $content_type;
    public function __construct($content_type)
    {
        $this->content_type = $content_type;
    }

    public function response(string $filepath, string $filename)
    {

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-type: ".$this->content_type);
        header("Content-Disposition: attachment; filename=$filename");

        readfile(DIR_UPLOAD.DIRECTORY_SEPARATOR.$filepath);

    }
}