<?php

namespace Services\File;

class FileUploader
{
    private string $error = '';
    private array $allowed_extensions = ['csv'];
    private array $allowed_mimes = ['text/csv'];

    /**
     * @throws \Exception
     */
    public function upload(): string
    {
        $this->validate();
        $filename = uniqid(rand(), true);
        move_uploaded_file($_FILES['file']['tmp_name'], DIR_UPLOAD .DIRECTORY_SEPARATOR. $filename);
        return $filename;
    }

    private function validate(): void
    {
        if(empty(['name']) || !is_file($_FILES['file']['tmp_name'])) {
            throw new \Exception('Файл не был загружен на сервер') ;
        }

        if ($_FILES['file']['error'] != UPLOAD_ERR_OK) {
            throw new \Exception('Ошибка загрузки файла: ERR_CODE:'.$_FILES['file']['error']);
        }

        $filename = html_entity_decode($_FILES['file']['name'], ENT_QUOTES, 'UTF-8');

        if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $this->allowed_extensions)) {
            throw new \Exception( 'Не верное расширение файла');
        }

        $filetype = html_entity_decode($_FILES['file']['type'], ENT_QUOTES, 'UTF-8');

        if (!in_array($filetype, $this->allowed_mimes)) {
            throw new \Exception('Загруженный файл не является файлом csv');
        }
    }
}