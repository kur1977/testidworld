<?php

namespace Services\Job;


use Services\Db\DB;

class Worker

{
    const STATUS_CREATED = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_FAILED = 3;
    const STATUS_SUCCESS = 4;

    const STATUSES = [
        self::STATUS_CREATED => 'создано',
        self::STATUS_PROCESSING => 'работает',
        self::STATUS_FAILED => 'ошибка',
        self::STATUS_SUCCESS => 'завершено',
    ];
    protected DB $db;
    public function __construct(DB $db)
    {
        $this->db = $db;
    }
    public function add(string $filename): int
    {
        $query = $this->db->query("INSERT INTO jobs SET file='".$this->db->escape($filename)."', status='".self::STATUS_CREATED."'");
        return $this->db->getLastId();
    }

    public function getInWork(): int
    {
        $query = $this->db->query("SELECT id FROM jobs WHERE status='".self::STATUS_PROCESSING."' LIMIT 1");
        return $query->num_rows;
    }

    public function setInWork(int $id): void
    {
        $this->db->query("UPDATE jobs SET status='".self::STATUS_PROCESSING."' WHERE id='".(int)$id."'");
    }

    public function getInfo(array $jobs): array
    {

        $query = $this->db->query("SELECT * FROM jobs WHERE id IN(".$this->db->escape(implode(',',$jobs)).") ORDER by id");

        array_walk($query->rows, function(&$value) {
            unset($value['file']);
        });

        return $query->rows;
    }

    public function getById(int $id): array
    {
        $query = $this->db->query("SELECT id, file FROM jobs WHERE id='".(int)$id."'");
        return $query->row;
    }

    public function getToImport(): array
    {
        $query = $this->db->query("SELECT id, file FROM jobs WHERE status='".self::STATUS_CREATED."' ORDER BY id LIMIT 1");
        return $query->row;
    }

    public function setDone(int $id, array $counters): void
    {
        $counters_arr = [];
        foreach($counters as $key=>$val){
            $counters_arr[]= $key."='".$this->db->escape($val)."'";
        }



        $this->db->query("UPDATE jobs SET status='".self::STATUS_SUCCESS."',
         ".implode(', ', $counters_arr)." WHERE id='".(int)$id."'");

    }
}