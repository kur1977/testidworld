<?php

namespace Services\Import;

interface ImportInterface
{
    public function import(): array;
    public function doImport(): void;
}