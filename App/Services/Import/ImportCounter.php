<?php

namespace Services\Import;

final class ImportCounter
{
    private array $counters = [
        'inserted'=>0,
        'updated'=>0,
        'errors'=>0,
    ];

    public function insert(){
        $this->counters['inserted']++;
    }

    public function update(){
        $this->counters['updated']++;
    }

    public function error(){
        $this->counters['errors']++;
    }

    public function getInfo(){
        return $this->counters;
    }
}