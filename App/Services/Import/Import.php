<?php

namespace Services\Import;

use Services\Db\DB;
use Services\Job\Worker;

abstract class Import implements ImportInterface
{
    protected $worker;
    protected $db;
    public function __construct(DB $db)
    {
        $this->db = $db;
        $this->worker = new Worker($this->db);
    }

    public function import(): array
    {
        if(!empty($_POST['jobs'])) {
            if (!$this->worker->getInWork()) {
                $this->doImport();
            }

            return $this->worker->getInfo($_POST['jobs']);
        }
    }


}