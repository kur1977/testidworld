<?php

namespace Services\Import;

use Services\Db\DB;

class ImportBackuper
{
    private $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function doBackup(): void
    {
        if(!empty($_SESSION['has_backup'])) return;

        $this->db->query("TRUNCATE backup_products");
        $this->db->query("INSERT INTO backup_products SELECT * FROM products");
        $_SESSION['has_backup'] = true;
    }
    private function hasBackup(): bool
    {
        $query = $this->db->query("SELECT sku FROM backup_products LIMIT 1");
        return (bool)$query->num_rows;
    }

    public function restoreBackup(): void
    {
        if(!$this->hasBackup()) return;

        $this->db->query("TRUNCATE products");
        $this->db->query("INSERT INTO products SELECT * FROM backup_products");

        unset($_SESSION['has_backup']);
    }
}