<?php

namespace Services\Import\Providers;

use Services\Import\Import;
use Services\Import\ImportBackuper;
use Services\Import\ImportCounter;

class CsvImportProvider extends Import
{
    const COLUMNS = [
        0 => 'sku',
        1 => 'price',
        2 => 'name',
        3 => 'description'
    ];
    public function doImport(): void
    {
       $job = $this->worker->getToImport();
       if($job){
           $import_backuper = new ImportBackuper($this->db);
           $import_backuper->doBackup();

           $this->worker->setInWork($job['id']);
            $importCounter = new ImportCounter();
           foreach($this->csv_read(DIR_UPLOAD.DIRECTORY_SEPARATOR.$job['file']) as $item){
                if(sizeof($item) != sizeof(self::COLUMNS)){
                    throw new \Exception("В файле ".sizeof($item)." столблов, а должно быть ".sizeof(self::COLUMNS));
                }

                if(!$this->validate($item)){
                    $importCounter->error();
                }

                $insert = [];
                foreach(self::COLUMNS as $key => $column){
                    $insert[]= $column."='".$this->db->escape($item[$key])."'";
                }

               $insert = implode(', ', $insert);

                $query = $this->db->query("INSERT INTO products SET ".$insert."
                 ON DUPLICATE KEY UPDATE ".$insert);

                if($this->db->countAffected() == 1){
                    $importCounter->insert();
                }elseif($this->db->countAffected() == 2){
                    $importCounter->update();
                }

           }

           $this->worker->setDone($job['id'], $importCounter->getInfo());
       }
    }

    private function csv_read($filename, $delimeter=',')
    {

        # tip: dont do that every time calling csv_read(), pass handle as param instead ;)
        $handle = fopen($filename, "r");

        if ($handle === false) {
            return false;
        }

        while (($data = fgetcsv($handle, 0, $delimeter)) !== false) {

            yield ($data);

        }
        fclose($handle);
    }

    private function validate(array $item): bool
    {
        //здесь можно как-то валидировать данные при необходимости
        return true;
    }
}