<?php
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'helpers.php';

spl_autoload_register(function($class) {
    $filename = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    require_once($filename);
});
session_start();

$db = new \Services\Db\DB('mysqli', DB_HOST, DB_USERNAME, DB_PAWWSORD, DB_DATABASE, DB_PORT );

$controller = new \Controllers\ImportController($db);


$mode = $_GET['mode'] ?? 'index';

switch ($mode){
    case 'index':
        $controller->index();
        break;
    case 'upload':
        $controller->upload();
        break;
    case 'import':
        $controller->import();
        break;
    case 'restore':
        $controller->restore();
        break;
    case 'download':
        $controller->download();
        break;
    default:
        $not_found_controller = new \Controllers\NotFoundController();
        $not_found_controller->index();
        break;
}