<?php

namespace Controllers;

use Factories\ImportFactory;
use Services\File\FileUploader;
use Services\File\FileResponder;
use Services\Import\ImportBackuper;
use Services\Import\Providers\CsvImportProvider;
use Services\Job\Worker;

class ImportController extends BaseController
{
  public function index(){
      require_once (DIR_VIEWS.'/index.php');
  }

  //загрузка файла
  public function upload(){
      $json = [];

      //FileUploader реализует загрузку файла на сервер
      $file_uploader = new FileUploader();

      try{
          //класс Worker реализует методы управления джобами. Получает и изменяет данные в табличке jobs
          $worker = new Worker($this->db);
          $filename = $file_uploader->upload();
          $job_id = $worker->add( $filename );
          $json['job_id'] = $job_id;
      }catch(\Exception $e){
          $json['error'] = $e->getMessage();
      }

      //юзаем хелпер ))
      json_output($json);
  }

    //Запуск импорта
  public function import()
  {
      try{
          //Используем фабрику. Задел на возможные реализации импорта например из XLS
          $import = ImportFactory::create('csv', $this->db);
          $json['info'] = $import->import();
      }catch(\Exception $e){
          $json['error'] = $e->getMessage();
      }

      json_output($json);
  }

  //Метод отдает файл в браузер. Файлы обычно храню вне доступа вебсервера.
    public function download()
    {
        if(!empty($_GET['id'])) {
            $worker = new Worker($this->db);
            $job = $worker->getById($_GET['id']);
            if($job){
                $responder = new FileResponder('text/csv');
                $responder->response($job['file'], 'import.csv');
            }
        }
    }

    //Восстанавливаем из бекапа
    public function restore()
    {
        $import_backuper = new ImportBackuper($this->db);
        $import_backuper->restoreBackup();

    }
}