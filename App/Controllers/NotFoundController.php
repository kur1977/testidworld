<?php

namespace Controllers;

class NotFoundController
{
    public function index(){
        require_once DIR_VIEWS . '/404.php';
    }
}