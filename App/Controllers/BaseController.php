<?php

namespace Controllers;

use Services\Db\DB;

class BaseController
{
    protected $db;
    public function __construct(DB $db)
    {
        $this->db = $db;
    }
}