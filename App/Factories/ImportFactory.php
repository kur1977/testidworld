<?php

namespace Factories;

use mysql_xdevapi\Exception;
use Services\Db\DB;
use Services\Import\Import;
use Services\Import\Providers\CsvImportProvider;

class ImportFactory
{
    public static function create(string $type, DB $db): Import
    {
        switch(strtolower($type)){
            case 'csv':
                $instance = new CsvImportProvider($db);
                break;
            default:
                throw new Exception($type.' import provider not found');
        }
        return $instance;
    }
}