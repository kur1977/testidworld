<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Импорт CSV</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
          rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
          crossorigin="anonymous">
    <link href="/css/stylesheet.css" rel="stylesheet" />
</head>
<body>

<div class="container text-center">
    <div class="row">
        <div class="col mb-3">
            <h1>Импорт CSV</h1>
        </div>
        <form>
            <div class="mb-3">
                <input id="file" type="file" accept=".csv" class="form-control" id="inputFile" aria-describedby="fileHelp">
                <div id="fileHelp" class="form-text">Загрузите CSV файл.(Артикул, цена, название, описание)</div>
            </div>

            <button id="sendFile" type="button" class="btn btn-primary" disabled>Загрузить</button>
            <button id="restore" type="button" class="btn btn-default" disabled>Отменить импорт</button>
            <div class="mt-3" id="fileList">

            </div>
        </form>
    </div>
</div>

<div>

</div>
<script
        src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo="
        crossorigin="anonymous"></script>

<script src="/js/script.js"></script>
</body>
</html>